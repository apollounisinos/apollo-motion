package apollo.mota.unisinos.br.apollo.bean;

import java.util.Date;

/**
 * Created 05/05/2018
 *
 * @author Bruno Mota [motaalvesb@gmail.com]
 */
public class MonitoramentoUsuario {

    private Integer monitoramentoUsuarioID;
    private Integer usuarioID;
    private Double moduloAceleracao;
    private Date datahora;
    private Float xInclinacao;
    private Float yInclinacao;
    private Float zInclinacao;
    private Float xAceleracao;
    private Float yAceleracao;
    private Float zAceleracao;

    public Integer getMonitoramentoUsuarioID() {
        return monitoramentoUsuarioID;
    }

    public void setMonitoramentoUsuarioID(Integer monitoramentoUsuarioID) {
        this.monitoramentoUsuarioID = monitoramentoUsuarioID;
    }

    public Integer getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Integer usuarioID) {
        this.usuarioID = usuarioID;
    }

    public Double getModuloAceleracao() {
        return moduloAceleracao;
    }

    public void setModuloAceleracao(Double moduloAceleracao) {
        this.moduloAceleracao = moduloAceleracao;
    }

    public Date getDatahora() {
        return datahora;
    }

    public void setDatahora(Date datahora) {
        this.datahora = datahora;
    }

    public Float getxInclinacao() {
        return xInclinacao;
    }

    public void setxInclinacao(Float xInclinacao) {
        this.xInclinacao = xInclinacao;
    }

    public Float getyInclinacao() {
        return yInclinacao;
    }

    public void setyInclinacao(Float yInclinacao) {
        this.yInclinacao = yInclinacao;
    }

    public Float getzInclinacao() {
        return zInclinacao;
    }

    public void setzInclinacao(Float zInclinacao) {
        this.zInclinacao = zInclinacao;
    }

    public Float getxAceleracao() {
        return xAceleracao;
    }

    public void setxAceleracao(Float xAceleracao) {
        this.xAceleracao = xAceleracao;
    }

    public Float getyAceleracao() {
        return yAceleracao;
    }

    public void setyAceleracao(Float yAceleracao) {
        this.yAceleracao = yAceleracao;
    }

    public Float getzAceleracao() {
        return zAceleracao;
    }

    public void setzAceleracao(Float zAceleracao) {
        this.zAceleracao = zAceleracao;
    }

}

