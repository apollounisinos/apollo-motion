package apollo.mota.unisinos.br.apollo.task;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;

import java.util.Calendar;

import apollo.mota.unisinos.br.apollo.MainActivity;
import apollo.mota.unisinos.br.apollo.PlayerExecuter;
import apollo.mota.unisinos.br.apollo.resource.ApolloClientWS;
import apollo.mota.unisinos.br.apollo.util.SMSUtil;

public class PredictionTask extends AsyncTask<Object, String, Void> {

    @SuppressLint("MissingPermission")
    @Override
    public Void doInBackground(Object... objs) {
        Context context = (Context) objs[0];
        Integer userID = (Integer) objs[1];

        //CASE USER ARE NOT IN RISK ----------------------------------------------------------------
        if (!MainActivity.inRisk) {
            Integer code = new ApolloClientWS().sendGET(userID);
            MainActivity.predictionCode = code + "";
            //Interact with sound
            if (MainActivity.makeSound) {
                if (code == 0) {
                    //No risk
                } else if (code == 1) {
                   //Execute nothing
                } else if (code == 2) {
                    new PlayerExecuter().executeMediumRisk(context);
                } else if (code == 3) {
                    MainActivity.feedback = false;
                    MainActivity.inRisk = true;
                    MainActivity.inRiskTime = Calendar.getInstance();
                    MainActivity.inRiskTime.add(Calendar.SECOND, 20);
                    new PlayerExecuter().executeHighRisk(context);
                }
            }
            //CASE USER ARE ALREADY IN RISK --------------------------------------------------------
        } else {
            //Verify user feedback
            if (MainActivity.feedback) {
                //Send it back to the server
                new ApolloClientWS().predictionFeedback(true);
                //Clear the inRisk variables
                MainActivity.inRisk = false;
                MainActivity.inRiskTime = null;
                //Vibrate to notify the user
                Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    v.vibrate(VibrationEffect.createOneShot(3000, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    v.vibrate(3000);
                }
                //keep going
            } else if (MainActivity.inRiskTime.getTimeInMillis() < Calendar.getInstance().getTimeInMillis()){
                //Zerate in risk variables
                MainActivity.inRisk = false;
                MainActivity.inRiskTime = null;
                //create call to rescue server
                new ApolloClientWS().predictionFeedback(false);
                //Send SMS
                Handler mHandler = new Handler(Looper.getMainLooper());

                Runnable myRunnable = new Runnable() {
                    public void run() {
                        new SMSUtil().sendSMS("5551991691958",
                                "Apollo EFP - Emergencia! Teste pode ter sido vitima de uma queda. " +
                                        "Verifique imediatamente se o teste precisa de ajuda.");
                    }
                };
                //Call phone emergency
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "5551981869160"));
                context.startActivity(intent);


                mHandler.post(myRunnable);
                //Send it back to the server
                //stop app
            }
        }
        return null;
    }
}
