package apollo.mota.unisinos.br.apollo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import apollo.mota.unisinos.br.apollo.bean.MonitoramentoUsuario;
import apollo.mota.unisinos.br.apollo.resource.ApolloClientWS;
import apollo.mota.unisinos.br.apollo.util.SMSUtil;

public class MainActivity extends AppCompatActivity implements SensorEventListener, View.OnTouchListener {

    public static List<MonitoramentoUsuario> buffer = new ArrayList<>();
    private DecimalFormat twoPlaces = new DecimalFormat("#0.00");

    private boolean shortPress = false;
    private boolean longPress = false;

    private SensorManager sensorAccelerometer;
    public static Context context;
    public static Integer userID = 1;
    public static Calendar inRiskTime;
    public static float[] acceleration = new float[3], inclination = new float[3];
    public static double moduleAcc;

    TextView incX, incY, incZ, accX, accY, accZ, modAcc, prediction;
    Switch sPersist, sSound;

    private float gravity[], magnetic[], azimuth, pitch, roll;
    private float accels[] = new float[3], mags[] = new float[3], values[] = new float[3];

    public static boolean shouldPersist = false, makeSound = false, inRisk = false, feedback = false;
    public static String predictionCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTitle("Apollo EFP Motion");
        super.onCreate(savedInstanceState);
        context = this.getApplicationContext();
        setContentView(R.layout.activity_main);

        //Check permissions
        new SMSUtil().checkPermission(context, MainActivity.this);

        //Define components
        incX = findViewById(R.id.inc_x_value);
        incY = findViewById(R.id.inc_y_value);
        incZ = findViewById(R.id.inc_z_value);

        accX = findViewById(R.id.acc_x_value);
        accY = findViewById(R.id.acc_y_value);
        accZ = findViewById(R.id.acc_z_value);

        modAcc = findViewById(R.id.modacc_value);
        prediction = findViewById(R.id.prediction_value);

        sPersist = findViewById(R.id.sPersistencia);
        sSound = findViewById(R.id.sEmitir);

        //Get data from accelerometer
        sensorAccelerometer = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensorAccelerometer.registerListener(this,
                sensorAccelerometer.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        //Get data from magnetic field
        sensorAccelerometer = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensorAccelerometer.registerListener(this,
                sensorAccelerometer.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD),
                SensorManager.SENSOR_DELAY_NORMAL);

        //Get data from type linear acceleration
        sensorAccelerometer = (SensorManager)getSystemService(SENSOR_SERVICE);
        sensorAccelerometer.registerListener(this,
                sensorAccelerometer.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_NORMAL);

        //Start thread
        this.saveBuffer();
        new ApolloClientWS().startSending();
        new ApolloClientWS().startListening();

        this.updateInterface();



        //Control the switch for persistence
        sPersist.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    shouldPersist = true;
                } else {
                    shouldPersist = false;
                }
            }
        });

        sSound.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    makeSound = true;
                } else {
                    makeSound = false;
                }
            }
        });
    }



    public void onAccuracyChanged(Sensor sensor, int accuracy){
    }

    public void saveBuffer() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        Thread.sleep(1000);
                        MonitoramentoUsuario mu = new MonitoramentoUsuario();
                        mu.setDatahora(new Date());
                        mu.setModuloAceleracao(moduleAcc);
                        mu.setUsuarioID(1);
                        mu.setxAceleracao(acceleration[0]);
                        mu.setyAceleracao(acceleration[1]);
                        mu.setzAceleracao(acceleration[2]);
                        mu.setxInclinacao(inclination[0]);
                        mu.setyInclinacao(inclination[1]);
                        mu.setzInclinacao(inclination[2]);
                        buffer.add(mu);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public void updateInterface() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (predictionCode.equals("0")) {
                                    prediction.setText("sem risco");
                                } else if (predictionCode.equals("1")) {
                                    prediction.setText("baixo");
                                } else if (predictionCode.equals("2")) {
                                    prediction.setText("medio");
                                } else if (predictionCode.equals("3")) {
                                    prediction.setText("alto");
                                }
                            }
                        });
                        Thread.sleep(1000);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public void onSensorChanged(SensorEvent event) {
        //Use the information to show on activity and send through ws
        if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            //Save data
            acceleration[0] = Float.parseFloat(twoPlaces.format(event.values[0]).replace(",",  "."));
            acceleration[1] = Float.parseFloat(twoPlaces.format(event.values[1]).replace(",",  "."));
            acceleration[2] = Float.parseFloat(twoPlaces.format(event.values[2]).replace(",",  "."));
            //Show it
            accX.setText(acceleration[0] + "");
            accY.setText(acceleration[1] + "");
            accZ.setText(acceleration[2] + "");

            //Calculate the module
            moduleAcc = Math.sqrt(Math.pow(acceleration[0], 2) + Math.pow(acceleration[1], 2) + Math.pow(acceleration[2], 2));
            moduleAcc = Double.parseDouble(twoPlaces.format(moduleAcc).replace(",", "."));
            //Show it
            modAcc.setText(moduleAcc + "");
        }

        //Work with inclination information
        switch (event.sensor.getType()) {
            case Sensor.TYPE_MAGNETIC_FIELD:
                mags = event.values.clone();
                break;
            case Sensor.TYPE_ACCELEROMETER:
                accels = event.values.clone();
                break;
        }

        if (mags != null && accels != null) {
            gravity = new float[9];
            magnetic = new float[9];
            SensorManager.getRotationMatrix(gravity, magnetic, accels, mags);
            float[] outGravity = new float[9];
            SensorManager.remapCoordinateSystem(gravity, SensorManager.AXIS_X,SensorManager.AXIS_Z, outGravity);
            SensorManager.getOrientation(outGravity, values);

            azimuth = values[0] * 57.2957795f;
            pitch = values[1] * 57.2957795f;
            roll = values[2] * 57.2957795f;
            mags = null;
            accels = null;

            inclination[0] = Float.parseFloat(twoPlaces.format(azimuth).replace(",", "."));
            inclination[1] = Float.parseFloat(twoPlaces.format(pitch).replace(",", "."));
            inclination[2] = Float.parseFloat(twoPlaces.format(event.values[2]).replace(",", "."));

            incX.setText(inclination[0] + "");
            incY.setText(inclination[1] + "");
            incZ.setText(inclination[2] + "");
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub

        if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){
            Toast.makeText(this, "SOS acionado", Toast.LENGTH_SHORT).show();
            //Long Press code goes here
            shortPress = false;
            longPress = true;
            System.out.println("--------- OK ------------");
            //Zerate in risk variables
            MainActivity.inRisk = false;
            MainActivity.inRiskTime = null;
            //Vibrate to notify the user
            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                v.vibrate(VibrationEffect.createOneShot(6000, VibrationEffect.DEFAULT_AMPLITUDE));
            } else {
                v.vibrate(6000);
            }
            //create call to rescue server
//            new ApolloClientWS().predictionFeedback(false);
            //Send SMS
            Handler mHandler = new Handler(Looper.getMainLooper());

            Runnable myRunnable = new Runnable() {
                public void run() {
                    new SMSUtil().sendSMS("5551991691958",
                            "Apollo EFP - Emergencia! Teste pode ter sido vitima de uma queda. " +
                                    "Verifique imediatamente se o teste precisa de ajuda.");
                }
            };
            //Call phone emergency
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "5551981869160"));
            context.startActivity(intent);

            mHandler.post(myRunnable);
            return true;
        }
        return super.onKeyLongPress(keyCode, event);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            event.startTracking();
            if (longPress == true) {
                shortPress = false;
            } else {
                feedback = true;
                shortPress = true;
                longPress = false;
                Toast.makeText(this, "Feedback enviado", Toast.LENGTH_SHORT).show();
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)) {
            if (shouldPersist) {
                sPersist.setChecked(false);
                shouldPersist = false;
            } else {
                sPersist.setChecked(true);
                shouldPersist = true;
                buffer.clear();
            }
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
