package apollo.mota.unisinos.br.apollo;

import android.content.Context;
import android.media.MediaPlayer;

public class PlayerExecuter {

    public void executeLowRisk(Context context) {
        try {
            MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.riscobaixo);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeMediumRisk(Context context) {
        try {
            MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.riscomedio);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void executeHighRisk(Context context) {
        try {
            MediaPlayer mediaPlayer = MediaPlayer.create(context, R.raw.riscoalto);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
