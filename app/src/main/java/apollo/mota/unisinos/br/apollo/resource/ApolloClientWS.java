package apollo.mota.unisinos.br.apollo.resource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;

import apollo.mota.unisinos.br.apollo.FormatterUtil;
import apollo.mota.unisinos.br.apollo.bean.MonitoramentoUsuario;
import apollo.mota.unisinos.br.apollo.task.AccelerometerTask;
import apollo.mota.unisinos.br.apollo.MainActivity;
import apollo.mota.unisinos.br.apollo.task.PredictionTask;

public class ApolloClientWS {
    private final String URL_PATH = "http://192.168.1.8:8080/ApolloWS";
    private final Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy HH:mm:ss").create();

    public void sendData(List<MonitoramentoUsuario> monitors) {
        String url = URL_PATH + "/monitoramentoUsuario/persist";
        URL urlConnection;
        HttpURLConnection connection = null;
        try {
            try {
                urlConnection = new URL(url);
                connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoOutput(true);
//                connection.setRequestProperty("usuarioID", 1 + "");
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                out.write(gson.toJson(monitors));
                out.close();

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println("Retorno: " + line);
//                        Fila temp = gson.fromJson(line, Fila.class);
//                        fila.setId(temp.getId());
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void predictionFeedback(Boolean feedback) {
        String url = URL_PATH + "/monitoramentoUsuario/predictionFeedback";
        URL urlConnection;
        HttpURLConnection connection = null;
        try {
            try {
                urlConnection = new URL(url);
                connection = (HttpURLConnection) urlConnection.openConnection();
                connection.setDoOutput(true);
                connection.setRequestProperty("userID", 1 + "");
                connection.setRequestProperty("feedback", feedback + "");
                connection.setRequestProperty("predictionID", 423 + "");
                OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                //out.write(gson.toJson(monitors));
                out.close();

                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        System.out.println("Retorno: " + line);
//                        Fila temp = gson.fromJson(line, Fila.class);
//                        fila.setId(temp.getId());
                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Integer sendGET(Integer userID) {
        try {
            URL obj = new URL(URL_PATH + "/monitoramentoUsuario/prediction");
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("userID", userID + "");
            int responseCode = con.getResponseCode();
            System.out.println("GET Response Code :: " + responseCode);
            if (responseCode == HttpURLConnection.HTTP_OK) { // success
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                // print result
                return Integer.parseInt(response.toString());
//                System.out.println(response.toString());
            } else {
                System.out.println("GET request not worked");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void startSending() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        if(MainActivity.shouldPersist == true) {
                            new AccelerometerTask().doInBackground(MainActivity.buffer);
                            //Then clear the buffer space
                            MainActivity.buffer.clear();
                        }
                        Thread.sleep(5000);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public void startListening() {
        Thread thread = new Thread(){
            public void run(){
                while (true) {
                    try {
                        new PredictionTask().doInBackground(MainActivity.context, MainActivity.userID);
                        Thread.sleep(5000);
                    } catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        thread.start();
    }

    public static void main(String[] args) {
        new ApolloClientWS().sendGET(1);
    }

}
