package apollo.mota.unisinos.br.apollo.util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;

import apollo.mota.unisinos.br.apollo.MainActivity;

public class SMSUtil extends AppCompatActivity {

    public void checkPermission(Context context, Activity activity) {
        //Request Permission
//        if (checkSelfPermission(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_DENIED) {
//            String[] permissions = {Manifest.permission.SEND_SMS};
//            requestPermissions(permissions, 1);
//        }

        if ((ActivityCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.SEND_SMS}, 100);
        }
        //Request Permission
//        if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED) {
//            String[] permissions = {Manifest.permission.READ_PHONE_STATE};
//            requestPermissions(permissions, 1);
//        }
        if ((ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, 100);
        }
    }

    public void checkPermissionDial(Context context, Activity activity) {
        //Request Permission
        if ((ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, 100);
        }
    }

    public void sendSMS(String phoneNumber, String message) {
        //Send the SMS
        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, null, null);
    }
}
