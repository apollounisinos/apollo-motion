package apollo.mota.unisinos.br.apollo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatterUtil {

    private final DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public String formatDateToString(Date date) {
        try {
            return format.format(date);
        } catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
