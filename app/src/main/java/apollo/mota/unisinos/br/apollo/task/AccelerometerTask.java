package apollo.mota.unisinos.br.apollo.task;

import android.os.AsyncTask;

import java.util.List;

import apollo.mota.unisinos.br.apollo.bean.MonitoramentoUsuario;
import apollo.mota.unisinos.br.apollo.resource.ApolloClientWS;

public class AccelerometerTask extends AsyncTask<List<MonitoramentoUsuario>, Double, Void> {

    @Override
    public Void doInBackground(List<MonitoramentoUsuario>... s) {
        new ApolloClientWS().sendData(s[0]);
        return null;
    }
}
